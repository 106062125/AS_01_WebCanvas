const canvas = document.getElementById('myCanvas');
const ctx = canvas.getContext('2d');
var filled = document.getElementById('filled');
var array = new Array();
var step = -1;
let x1= 0;
let y1= 0;
let x2= 0;
let y2= 0;
let x3= 0;
let y3= 0;
let isDrawing = false;
let isTyping = false;
let pencil = true;
let erase = false;
let flag=true;
let circle = false;
let triangle=false;
let rectangle=false;
let line=false;
let type=false;
let rem;
var input_arr = [];
const color = document.getElementById('colorpicker');
if(erase) color='white';

draw();
push_canvas();
function draw(){

    canvas.addEventListener('mouseup', function(e){ 
        isDrawing = false ;
        x3 = e.offsetX;
        y3 = e.offsetY;
        if(flag) {
            push_canvas();
            flag=false;
        }
        if(circle){
            ctx.fillStyle=ctx.strokeStyle;
            var a =Math.abs(x3-x1)/2;
            var b =Math.abs(y3-y1)/2;
            var radius = (a>b)?a:b;
            ctx.beginPath();
            ctx.arc((x1+x3)/2, (y1+y3)/2,radius , 0, 2 * Math.PI);
            ctx.stroke();
            if(filled.checked) ctx.fill();
            x1 = x3;
            y1 = y3;
        }
        else if(line){
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x3, y3);
            ctx.stroke();
            x1 = x3;
            y1 = y3;
        }
        else if(rectangle){
            var tempx=(x1<x3)? x1:x3;
            var tempy=(y1<y3)? y1:y3;
            ctx.fillStyle=ctx.strokeStyle;
            if(filled.checked) ctx.fillRect(tempx, tempy, Math.abs(x1-x3),Math.abs(y1-y3));
            else ctx.strokeRect(tempx, tempy, Math.abs(x1-x3),Math.abs(y1-y3));
            x1 = x3;
            y1 = y3;
        }
        else if(triangle){
            ctx.fillStyle=ctx.strokeStyle;
            var tempx=(x1<x3)? x1:x3;
            var tempy=(y1<y3)? y1:y3;
            var tempx2=(x1>x3)? x1:x3;
            var tempy2=(y1>y3)? y1:y3;
            var wid = Math.abs(x1-x3);
            var hei = Math.abs(y1-y3);
            ctx.beginPath();
            ctx.moveTo(tempx, tempy+hei);
            ctx.lineTo(tempx2, tempy2);
            ctx.lineTo(tempx+wid/2,tempy);
            ctx.closePath();
            ctx.stroke();
            if(filled.checked) ctx.fill();
            x1 = x3;
            y1 = y3;
            
        }
        else if (type){
            input_arr.length=0;
            isTyping=true;
            ctx.clearRect(0,0,800,590);
            var canvasPic = new Image();
            canvasPic.src = array[rem];
            canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
        }

    })
    canvas.addEventListener('mousedown', function(e){
        isDrawing = true;
        isTyping = false;
        x1 = e.offsetX;
        y1 = e.offsetY;
        flag=true;
        rem=step;
        //ctx.lineWidth = 5
        if(!erase) colorpicker(color.value);
        else ctx.strokeStyle ='white';
        ctx.lineCap = 'round';
        ctx.lineJoin = 'round';
        
    })

    canvas.addEventListener('mousemove', function(e){
        if(!isDrawing){
            return
        }
        x2 = e.offsetX;
        y2 = e.offsetY;

        if(pencil||erase){
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.stroke();
            x1 = x2;
            y1 = y2;
        }
        else if(circle){
            ctx.fillStyle=ctx.strokeStyle;
            var a =Math.abs(x2-x1)/2;
            var b =Math.abs(y2-y1)/2;
            var radius = (a>b)?a:b;
            ctx.clearRect(0,0,800,590);
            var canvasPic = new Image();
            canvasPic.src = array[rem];
            ctx.drawImage(canvasPic, 0, 0);
            ctx.beginPath();
            ctx.arc((x1+x2)/2, (y1+y2)/2,radius , 0, 2 * Math.PI);
            ctx.stroke();
            if(filled.checked) ctx.fill();
        }
        else if(rectangle){
            var tempx=(x1<x2)? x1:x2;
            var tempy=(y1<y2)? y1:y2;
            ctx.clearRect(0,0,800,590);
            var canvasPic = new Image();
            canvasPic.src = array[rem];
            ctx.drawImage(canvasPic, 0, 0);
            ctx.fillStyle=ctx.strokeStyle;
            if(filled.checked) ctx.fillRect(tempx, tempy, Math.abs(x1-x2),Math.abs(y1-y2));
            else ctx.strokeRect(tempx, tempy, Math.abs(x1-x2),Math.abs(y1-y2));
        }
        else if(line){
            ctx.clearRect(0,0,800,590);
            var canvasPic = new Image();
            canvasPic.src = array[rem];
            ctx.drawImage(canvasPic, 0, 0);
            ctx.beginPath();
            ctx.moveTo(x1, y1);
            ctx.lineTo(x2, y2);
            ctx.stroke();
            ctx.closePath();
        }
        else if(triangle){
            ctx.fillStyle=ctx.strokeStyle;
            ctx.clearRect(0,0,800,590);
            var canvasPic = new Image();
            canvasPic.src = array[rem];
            ctx.drawImage(canvasPic, 0, 0);
            var tempx=(x1<x2)? x1:x2;
            var tempy=(y1<y2)? y1:y2;
            var tempx2=(x1>x2)? x1:x2;
            var tempy2=(y1>y2)? y1:y2;
            var wid = Math.abs(x1-x2);
            var hei = Math.abs(y1-y2);
            ctx.beginPath();
            ctx.moveTo(tempx, tempy+hei);
            ctx.lineTo(tempx2, tempy2);
            ctx.lineTo(tempx+wid/2,tempy);
            ctx.closePath();
            ctx.stroke();
            if(filled.checked) ctx.fill();
        }
    })
    window.addEventListener('keypress',function(e){
        ctx.clearRect(0,0,800,590);
        var canvasPic = new Image();
        canvasPic.src = array[rem];
        ctx.drawImage(canvasPic, 0, 0);
        if(!isTyping) return ;
        if(!type) return;
        var word=e.which;
        input_arr.push(word);
        var charstr = String.fromCharCode.apply(null, input_arr);
        text_font();
        
        ctx.fillText(charstr,x1+2,y1+15);
        push_canvas();
    });
    
}




function Pencil(){
    colorpicker(color.value);
    pencil = true;
    circle=false;
    triangle=false;
    rectangle=false;
    line=false;
    erase=false;
    type=false;
    document.body.style.cursor="crosshair";
}
function Erase(){
    erase = true;
    pencil = false;
    circle=false;
    triangle=false;
    rectangle=false;
    line=false;
    type=false;
    document.body.style.cursor="cell";
}

function line_w(line_w){
    ctx.lineWidth = line_w;
}

function colorpicker(value){
    ctx.strokeStyle =value;
}

function clearcanva(){
    ctx.clearRect(0,0,800,590);
    push_canvas();
}

function type_text(){
    circle=false;
    triangle=false;
    rectangle=false;
    line=false;
    erase=false;
    pencil=false;
    type=true;
    document.body.style.cursor="crosshair";
}

function download_canvas(dl){
    var image = myCanvas.toDataURL("image/jpg");
    dl.href = image;
}

function load_canvas(input){
    var file = input.files[0];
    var src=URL.createObjectURL(file);
    var img =new Image();
    img.src=src;
    img.onload=function(){
        ctx.drawImage(this,0,0,800,590);
        push_canvas();
    }
    
}

function push_canvas() {
    step++;
    if (step < array.length) { array.length = step; }
    array.push(document.getElementById('myCanvas').toDataURL());
}

function redo_canvas() {
    if (step < array.length-1) {
        step++;
        var canvasPic = new Image();
        canvasPic.src = array[step];
        ctx.clearRect(0,0,800,590);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function undo_canvas() {
    if (step > 0) {
        step--;
        var canvasPic = new Image();
        canvasPic.src = array[step];
        ctx.clearRect(0,0,800,590);
        canvasPic.onload = function () { ctx.drawImage(canvasPic, 0, 0); }
    }
}

function drawcircle(){
    
    circle=true;
    triangle=false;
    rectangle=false;
    line=false;
    pencil=false;
    erase=false;
    type=false;
    document.body.style.cursor='url("circle_cursor.png"),auto';
    
}

function drawtriangle(){
    circle=false;
    triangle=true;
    rectangle=false;
    line=false;
    pencil=false;
    erase=false;
    type=false;
    document.body.style.cursor='url("triangle_cursor.png"),auto';
}

function drawrectangle(){
    circle=false;
    triangle=false;
    rectangle=true;
    line=false;
    pencil=false;
    erase=false;
    type=false;
    document.body.style.cursor='url("rect_cursor.png"),auto';
}

function drawline(){
    circle=false;
    triangle=false;
    rectangle=false;
    line=true;
    pencil=false;
    erase=false;
    type=false;
    document.body.style.cursor="crosshair";
}

function text_font(){
    ctx.fillStyle=ctx.strokeStyle;
    var selected=document.getElementById('font_sel');
    var size=document.getElementById('font_size');
    var fontValue = selected.options[selected.selectedIndex].text;
    var sizeValue = size.options[size.selectedIndex].text;
    ctx.font=sizeValue+"px "+fontValue;
}
